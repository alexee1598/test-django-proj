import logging

from rest_framework_simplejwt.backends import TokenBackend
from django.utils import timezone

from user.models import ApplicationUser

logger = logging.getLogger(__name__)


class LastUserActionMiddleware(object):
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)
        token = request.META.get('HTTP_AUTHORIZATION', ' ').split(' ')[1]
        if token:
            try:
                valid_data = TokenBackend(algorithm='HS256').decode(token, verify=False)
                ApplicationUser.objects.filter(id=valid_data['id']).update(last_request=timezone.now())
            except Exception as v:
                logger.info(f'validation error {v}')
        return response

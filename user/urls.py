from django.urls import path
from rest_framework import routers
from rest_framework_simplejwt.views import TokenRefreshView

from user import views

router = routers.DefaultRouter()
router.register('user', views.UserFilterCRUD, basename='user')

urlpatterns = [
    path('login/', views.LogInView.as_view(), name='login'),
    path('analitics/<int:id>', views.user_last_activity, name='user_last_activity'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
]

urlpatterns += router.urls

from rest_framework import mixins
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet
from rest_framework_simplejwt.views import TokenObtainPairView

from user.models import ApplicationUser
from user.serializers import LogInSerializer, ApplicationUserSerializer


class LogInView(TokenObtainPairView):
    serializer_class = LogInSerializer


class UserFilterCRUD(mixins.CreateModelMixin,
                     mixins.ListModelMixin,
                     GenericViewSet):
    serializer_class = ApplicationUserSerializer


@api_view(['GET'])
def user_last_activity(request, id: int) -> Response:
    u: ApplicationUser = ApplicationUser.objects.get(id=id)
    return Response({'last_request ': u.last_request, 'last_login': u.last_login})

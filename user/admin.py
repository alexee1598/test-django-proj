from django.contrib import admin

from user.models import ApplicationUser


@admin.register(ApplicationUser)
class LikePostAdmin(admin.ModelAdmin):
    list_display = ('id', 'username',)

import concurrent.futures
import json
import os
import random
import string

import requests

number_of_users: int = int(os.getenv('number_of_users', 3))
max_posts_per_user: int = int(os.getenv('max_posts_per_user', 3))
max_likes_per_user: int = int(os.getenv('max_likes_per_user', 3))


def create_user(user_data: dict[str, str]) -> int:
    response: requests.models.Response = requests.post(url='http://localhost:8000/api/user/', data=user_data)
    response_dict: json = json.loads(response.text)
    return response_dict.get('id')


def create_post(post_data: dict[str, str | int]) -> int:
    response: requests.models.Response = requests.post(url='http://localhost:8000/api/post/', data=post_data)
    response_dict: json = json.loads(response.text)
    return response_dict.get('id')


def create_like(post_data: dict[str, str | int]) -> None:
    requests.post(url='http://localhost:8000/api/like/', data=post_data)


def create_models() -> None:
    created_users_id: set[int] = set()
    created_posts_id: set[int] = set()

    with concurrent.futures.ThreadPoolExecutor(16) as executor:
        users: list[dict[str, str]] = []
        for i in range(int(number_of_users)):
            field = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(10))
            data = {
                'username': field,
                'password': field,
                'email': f'{field}@gmail.com',
                'first_name': field,
                'last_name': field,
            }
            users.append(data)
        created_users_id |= {r for r in executor.map(create_user, users) if r}

        for i in created_users_id:
            posts_data: list[dict[str, str | int]] = []
            for p in range(random.randint(1, max_posts_per_user)):
                posts_data.append(
                    {
                        'creator': i,
                        'title': ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(64)),
                    }
                )
            created_posts_id |= {r for r in executor.map(create_post, posts_data) if r}
        for i in created_users_id:
            like_data = []
            available_posts = created_posts_id.copy()
            for p in range(random.randint(1, max_likes_per_user)):
                if available_posts:
                    post = random.choice(tuple(available_posts))
                    available_posts.remove(post)
                    like_data.append({'user': i, 'post': post})
            executor.map(create_like, like_data)


create_models()
print('finished')

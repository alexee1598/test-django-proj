from django.db.models import Count
from rest_framework import viewsets
from rest_framework.decorators import api_view
from rest_framework.response import Response

from post.models import LikePost
from post.serializers import PostSerializer, LikePostSerializer, LikeAnalitics


class PostCRUD(viewsets.ModelViewSet):
    serializer_class = PostSerializer


class LikePostCRUD(viewsets.ModelViewSet):
    serializer_class = LikePostSerializer


@api_view(['GET'])
def likes_per_day(request):
    date_from = request.GET.get('date_from')
    date_to = request.GET.get('date_to')
    if date_to and date_from:
        return Response(
            LikeAnalitics(
                LikePost.objects.filter(created_at__gte=date_from, created_at__lte=date_to)
                .extra(select={'created': 'date(created_at)'})
                .values('created')
                .annotate(count=Count('created_at')),
                many=True,
            ).data
        )
    return Response({'message': 'return params'})

from rest_framework import serializers

from post.models import Post, LikePost


class PostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        read_only = ('id',)
        fields = '__all__'


class LikePostSerializer(serializers.ModelSerializer):
    class Meta:
        model = LikePost
        read_only = ('id',)
        fields = '__all__'


class LikeAnalitics(serializers.Serializer):
    created = serializers.DateField()
    count = serializers.IntegerField()

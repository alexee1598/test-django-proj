from django.contrib import admin

from post.models import Post, LikePost


@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    list_display = ('id', 'title',)


@admin.register(LikePost)
class LikePostAdmin(admin.ModelAdmin):
    list_display = ('id', 'created_at',)

from django.db import models

from user.models import ApplicationUser


class Post(models.Model):
    title = models.CharField(max_length=256)
    created_at = models.DateTimeField(auto_now_add=True)
    creator = models.ForeignKey(ApplicationUser, related_name='application_user', on_delete=models.CASCADE)

    class Meta:
        ordering = ('-id',)


class LikePost(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(ApplicationUser, related_name='user', on_delete=models.CASCADE)
    post = models.ForeignKey(Post, related_name='post', on_delete=models.CASCADE)

    class Meta:
        ordering = ('-id',)
        unique_together = (('user', 'post'),)

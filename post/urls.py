from django.urls import path
from rest_framework import routers

from post import views

router = routers.DefaultRouter()
router.register('post', views.PostCRUD, basename='post')
router.register('like', views.LikePostCRUD, basename='post_like')

urlpatterns = [
    path('analitics/', views.likes_per_day, name='likes_per_day'),
]

urlpatterns += router.urls
